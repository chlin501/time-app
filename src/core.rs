use time::{format_description::BorrowedFormatItem, macros::format_description, OffsetDateTime};
use time_tz::{Offset, TimeZone};

const RFC3339_FORMAT: &[BorrowedFormatItem] = format_description!(
    "[year]-[month]-[day]T[hour]:[minute]:[second][offset_hour \
        sign:mandatory]:[offset_minute]"
);

pub fn validate_timestamp(timestamp_str: &str) -> Result<OffsetDateTime, String> {
    OffsetDateTime::parse(timestamp_str, RFC3339_FORMAT)
        .map_err(|err| String::from(format!("Invalid rfc3339 timestamp format: {}", err)))
}

pub fn validate_cities(city_str: &str) -> Result<String, String> {
    time_tz::timezones::iter()
        .find(|tz| (*tz).name().contains(city_str))
        .map(|tz| (*tz).name().to_string())
        .ok_or_else(|| String::from(format!("Invalid city name: {city_str}")))
}

#[derive(Debug)]
pub struct Source {
    offset_date_time: OffsetDateTime,
    target_city: String,
}

impl Source {
    pub fn new(rfc3339_timestamp: OffsetDateTime, city_name: String) -> Self {
        Self {
            offset_date_time: rfc3339_timestamp,
            target_city: city_name,
        }
    }

    pub fn timestamp_of(&self, city: String) -> Option<OffsetDateTime> {
        time_tz::timezones::iter()
            .find(|tz| (*tz).name().to_lowercase().contains(&city.to_lowercase()))
            .map(|matched| {
                let now_utc = OffsetDateTime::now_utc();
                let city_offset = matched.get_offset_utc(&now_utc);
                self.offset_date_time.to_offset(city_offset.to_utc())
            })
    }

    pub fn target_city(&self) -> String {
        self.target_city.clone()
    }

    pub fn offset_date_time(&self) -> String {
        self.offset_date_time.to_string()
    }
}
