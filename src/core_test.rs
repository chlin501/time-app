#[cfg(test)]
mod core_test {
    use crate::core::Source;
    use time::macros::{datetime, offset};
    use time_tz::{
        timezones::db::{asia::TOKYO, europe::AMSTERDAM},
        TimeZone,
    };

    #[test]
    fn test_timestamp_conversion() {
        let taipei_offset_date_time = datetime!(2024-01-01 00:00:00 +8);
        let source = Source::new(taipei_offset_date_time);
        let tokyo = String::from(TOKYO.name());
        let tokyo_offset_date_time = source.timestamp_of(tokyo).unwrap();
        let taipei_in_tokyo_timestamp = taipei_offset_date_time
            .checked_to_offset(offset!(+9))
            .unwrap();
        assert_eq!(taipei_in_tokyo_timestamp, tokyo_offset_date_time);
        let amsterdam_offset_date_time = datetime!(2023-12-31 18:00:00 +2);
        let amsterdam = String::from(AMSTERDAM.name());
        let taipei_in_amsterdam_timestamp = source.timestamp_of(amsterdam).unwrap();
        assert_eq!(taipei_in_amsterdam_timestamp, amsterdam_offset_date_time);
    }
}
