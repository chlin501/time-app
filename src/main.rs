use clap::{
    error::{Error, ErrorKind::ValueValidation},
    Arg, Command,
};
use core::Source;
use std::process::exit;
use time::OffsetDateTime;

mod core;
#[cfg(test)]
mod core_test;

fn get_args() -> Result<Source, Error> {
    let cmd = Command::new("time-app")
        .arg(
            Arg::new("timestamp")
                .long("timestamp")
                .required(true)
                .value_parser(core::validate_timestamp),
        )
        .arg(
            Arg::new("city")
                .long("city")
                .required(true)
                .value_parser(core::validate_cities),
        );

    cmd.try_get_matches().map(|matched| {
        let rfc3339_timestamp = matched.get_one::<OffsetDateTime>("timestamp").unwrap();
        let city_name = matched.get_one::<String>("city").unwrap();
        Source::new(*rfc3339_timestamp, (*city_name).clone())
    })
}

fn main() {
    let result = get_args()
        .map(|source| {
            source
                .timestamp_of(source.target_city())
                .map(|target_offset_date_time| {
                    format!("target offset date time: {}", target_offset_date_time)
                })
                .unwrap()
        })
        .map_err(|error| {
            let error_kind = error.kind();
            match error_kind {
                ValueValidation => {
                    let msg: Vec<String> = error
                        .context()
                        .map(|(ctx_kind, ctx_value)| {
                            format!(
                                "{} {}",
                                ctx_kind.to_string().to_lowercase(),
                                ctx_value.to_string().to_lowercase()
                            )
                        })
                        .collect();
                    format!("Fail validating values: {:?}", msg.join(" with "))
                }
                _ => format!("Error: {:?}", error_kind),
            }
        });
    match result {
        Ok(success) => {
            println!("{:?}", success);
            exit(0)
        }
        Err(error) => {
            eprintln!("{:?}", error);
            exit(1)
        }
    }
}
